\contentsline {chapter}{\numberline {1}Newton Mechanik}{5}
\contentsline {section}{\numberline {1.1}Formale Grundlagen}{5}
\contentsline {subsection}{\numberline {1.1.1}Raum-Zeit}{5}
\contentsline {subsection}{\numberline {1.1.2}Galilei Transformationen}{8}
\contentsline {section}{\numberline {1.2}Newton Axiome}{10}
\contentsline {section}{\numberline {1.3}Analyse der Newton Gesetze}{16}
\contentsline {subsection}{\numberline {1.3.1}Invarianzen und Symmetrien}{16}
\contentsline {subsection}{\numberline {1.3.2}Erhaltungss\active@dq \dq@prtct {a}tze}{19}
\contentsline {subsubsection}{Impuls}{20}
\contentsline {subsubsection}{Drehimpuls}{21}
\contentsline {subsubsection}{Energie}{23}
\contentsline {subsection}{\numberline {1.3.3}Allgemeines zur L\active@dq \dq@prtct {o}sung}{28}
\contentsline {section}{\numberline {1.4}Beispiele und Anwendungen}{35}
\contentsline {subsection}{\numberline {1.4.1}Zweik\active@dq \dq@prtct {o}rper Zentralkraft Problem (allgemein)}{35}
\contentsline {subsubsection}{Reduktion der Freiheitsgrade I: Gesamtimpulserhaltung}{36}
\contentsline {subsubsection}{Reduktion der Freiheitsgrade II: Drehimpulserhaltung}{37}
\contentsline {subsubsection}{Diskussion des Radialproblems}{39}
\contentsline {subsection}{\numberline {1.4.2}Keplerproblem}{42}
\contentsline {subsubsection}{Negative Energie: Gebundene Bewegung}{44}
\contentsline {subsubsection}{Positive Energie: Ungebundene Bewegung}{46}
\contentsline {subsection}{\numberline {1.4.3}Allgemeine Charakerisierung von Bewegungen (in Einer Dimension)}{48}
\contentsline {subsection}{\numberline {1.4.4}Kleine Schwingungen}{53}
\contentsline {subsubsection}{Zwei Wagen auf einer Schiene}{56}
\contentsline {section}{\numberline {1.5}Kr\active@dq \dq@prtct {a}fte}{58}
\contentsline {subsection}{\numberline {1.5.1}Geschwindigkeitsabh\active@dq \dq@prtct {a}ngige Kr\active@dq \dq@prtct {a}fte}{58}
\contentsline {subsubsection}{Beschleunigungskr\active@dq \dq@prtct {a}fte}{59}
\contentsline {subsubsection}{Reibungskr\active@dq \dq@prtct {a}fte}{59}
\contentsline {subsubsection}{Lorentzkraft}{60}
\contentsline {subsection}{\numberline {1.5.2}Zeitabh\active@dq \dq@prtct {a}ngige Kr\active@dq \dq@prtct {a}fte}{60}
\contentsline {subsection}{\numberline {1.5.3}Zwangskr\active@dq \dq@prtct {a}fte}{61}
\contentsline {section}{\numberline {1.6}Zusammenfassung}{68}
\contentsline {chapter}{\numberline {2}Lagrange Mechanik}{69}
\contentsline {section}{\numberline {2.1}Ableitung I: d'Alembert'sches Prinzip}{69}
\contentsline {subsection}{\numberline {2.1.1}Formulierung des d'Alembert'schen Prinzips}{71}
\contentsline {subsection}{\numberline {2.1.2}Herleitung der Lagrange Gleichung}{73}
\contentsline {section}{\numberline {2.2}Ableitung II: Variationsprinzipien}{78}
\contentsline {subsection}{\numberline {2.2.1}Exkurs \active@dq \dq@prtct {u}ber Variationsprinzipien}{78}
\contentsline {subsection}{\numberline {2.2.2}Hamilton'sches Extremalprinzip und Weitere Begriffsbildung}{85}
\contentsline {section}{\numberline {2.3}\active@dq \dq@prtct {U}ber die Bedeutung des Hamiltonschen Extremalprinzips}{87}
\contentsline {subsection}{\numberline {2.3.1}Koordinateninvarianz der Lagrange Gleichungen}{88}
\contentsline {section}{\numberline {2.4}Noether Theorem}{92}
\contentsline {section}{\numberline {2.5}Zusammenfassung}{96}
\contentsline {chapter}{\numberline {3}Der Starre K\active@dq \dq@prtct {o}rper}{97}
\contentsline {section}{\numberline {3.1}Definition des Starren K\active@dq \dq@prtct {o}rpers}{97}
\contentsline {section}{\numberline {3.2}Bewegte Bezugssysteme}{100}
\contentsline {subsection}{\numberline {3.2.1}Koordinatentransformationen}{100}
\contentsline {subsubsection}{Translatorische Bewegung}{102}
\contentsline {subsubsection}{Rotation}{102}
\contentsline {subsubsection}{Allgemeiner Fall}{104}
\contentsline {subsection}{\numberline {3.2.2}Beschleunigungs-, Zentrifugal- und Corioliskr\active@dq \dq@prtct {a}fte}{104}
\contentsline {subsubsection}{Translatorische Bewegung}{104}
\contentsline {subsubsection}{Rotation}{105}
\contentsline {section}{\numberline {3.3}Lagrangefunktion des Starren K\active@dq \dq@prtct {o}rpers}{107}
\contentsline {section}{\numberline {3.4}Tr\active@dq \dq@prtct {a}gheitstensor und Eulergleichungen}{109}
\contentsline {subsection}{\numberline {3.4.1}Analyse des Tr\active@dq \dq@prtct {a}gheitstensors}{110}
\contentsline {subsection}{\numberline {3.4.2}Ableitung der Bewegungsgleichungen}{113}
\contentsline {section}{\numberline {3.5}Schwerer Kreisel}{115}
\contentsline {subsection}{\numberline {3.5.1}Lagrangefunktion des Schweren Kreisels}{118}
\contentsline {subsection}{\numberline {3.5.2}Diskussion der Kreiselbewegung}{122}
\contentsline {section}{\numberline {3.6}Zusammenfassung}{126}
\contentsline {chapter}{\numberline {4}Hamilton Mechanik}{129}
\contentsline {section}{\numberline {4.1}Hamilton Funktion und Hamilton Bewegungsgleichungen}{130}
\contentsline {subsection}{\numberline {4.1.1}Herleitung der Hamilton Gleichungen}{130}
\contentsline {subsection}{\numberline {4.1.2}Praktisches zum \active@dq \dq@prtct {U}bergang Lagrange $\to $ Hamilton}{134}
\contentsline {section}{\numberline {4.2}Formale Mechanik}{137}
\contentsline {subsection}{\numberline {4.2.1}Struktur der Hamilton Gleichungen}{138}
\contentsline {subsection}{\numberline {4.2.2}Symplektische Struktur des Phasenraums}{141}
\contentsline {subsection}{\numberline {4.2.3}Poisson Klammern}{143}
\contentsline {subsection}{\numberline {4.2.4}Variationsprinzip}{146}
\contentsline {subsection}{\numberline {4.2.5}Kanonische Transformationen I: Definition}{147}
\contentsline {subsection}{\numberline {4.2.6}Kanonische Transformationen II: Erzeugende}{151}
\contentsline {subsubsection}{Variable ${\bf q}$ und ${\bf Q}$ unabh\active@dq \dq@prtct {a}ngig}{152}
\contentsline {subsubsection}{Variable ${\bf q}$ und ${\bf P}$ unabh\active@dq \dq@prtct {a}ngig}{153}
\contentsline {subsubsection}{Variable ${\bf p}$ und ${\bf P}$ unabh\active@dq \dq@prtct {a}ngig}{154}
\contentsline {subsubsection}{Variable ${\bf p}$ und ${\bf Q}$ unabh\active@dq \dq@prtct {a}ngig}{154}
\contentsline {subsection}{\numberline {4.2.7}Kanonische Transformationen III: Kriterien f\active@dq \dq@prtct {u}r Kanonizit\active@dq \dq@prtct {a}t}{156}
\contentsline {subsection}{\numberline {4.2.8}Liouville'scher Satz}{159}
\contentsline {section}{\numberline {4.3}Zusammenfassung}{162}
\contentsline {chapter}{\numberline {5}Stabilit\active@dq \dq@prtct {a}t und Chaos}{165}
\contentsline {section}{\numberline {5.1}Beispiele Nichtintegrablen Verhaltens}{166}
\contentsline {subsection}{\numberline {5.1.1}Das Henon-Heiles System}{166}
\contentsline {subsection}{\numberline {5.1.2}Nichtintegrabilit\active@dq \dq@prtct {a}t im Sonnensystem}{169}
\contentsline {section}{\numberline {5.2}Lineare Stabilit\active@dq \dq@prtct {a}tstheorie}{175}
\contentsline {section}{\numberline {5.3}Theorie von Hamilton-Jacobi}{182}
\contentsline {section}{\numberline {5.4}Integrable Systeme}{187}
\contentsline {section}{\numberline {5.5}Winkel-Wirkungsvariable}{190}
\contentsline {subsection}{\numberline {5.5.1}Winkel-Wirkungsvariable f\active@dq \dq@prtct {u}r Systeme mit $f=1$}{192}
\contentsline {subsection}{\numberline {5.5.2}Winkel-Wirkungsvariable f\active@dq \dq@prtct {u}r Systeme mit $f\ge 1$}{195}
\contentsline {section}{\numberline {5.6}St\active@dq \dq@prtct {o}rung Integrabler Systeme}{200}
\contentsline {subsection}{\numberline {5.6.1}St\active@dq \dq@prtct {o}rungstheorie}{200}
\contentsline {subsection}{\numberline {5.6.2}KAM-Theorem}{203}
\contentsline {section}{\numberline {5.7}Nichtintegrable Systeme}{204}
\contentsline {subsection}{\numberline {5.7.1}Twist Maps}{205}
\contentsline {subsection}{\numberline {5.7.2}Poincar\'e-Birkhoff Fixpunkt Theorem}{207}
